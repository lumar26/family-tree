package view;

import controller.Controller;
import model.*;
import model.enums.Gender;
import view.table_model.FamilyMembersTableModel;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewMemberForm extends javax.swing.JDialog {

    private Membership newMembership;
    private final User user;
    private Controller controller;
    private FamilyMembersTableModel model;

    public NewMemberForm(java.awt.Frame parent, boolean modal, User u) {
        super(parent, modal);
        user = u;
        try {
            controller = Controller.getInstance();
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(NewMemberForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();
        prepareComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpLivingStatus = new javax.swing.ButtonGroup();
        btnGroupGender = new javax.swing.ButtonGroup();
        pnlNewMemberData = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblSurname = new javax.swing.JLabel();
        lblGender = new javax.swing.JLabel();
        lblBirthDate = new javax.swing.JLabel();
        lblBirthPlace = new javax.swing.JLabel();
        lblBiography = new javax.swing.JLabel();
        lblPicture = new javax.swing.JLabel();
        checkAlive = new javax.swing.JCheckBox();
        checkDead = new javax.swing.JCheckBox();
        layerSpecificInfo = new javax.swing.JLayeredPane();
        pnlSpecificAlive = new javax.swing.JPanel();
        lblAddress = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        pnlSpecificDead = new javax.swing.JPanel();
        lblDeathDate = new javax.swing.JLabel();
        lblDeathPlace = new javax.swing.JLabel();
        txtDeathPlace = new javax.swing.JTextField();
        txtDeathDate = new javax.swing.JFormattedTextField();
        lblDateMessage = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtSurname = new javax.swing.JTextField();
        txtBirthPlace = new javax.swing.JTextField();
        txtUrlBiography = new javax.swing.JTextField();
        txtUrlPicture = new javax.swing.JTextField();
        checkMale = new javax.swing.JCheckBox();
        checkFemale = new javax.swing.JCheckBox();
        txtBirthDate = new javax.swing.JFormattedTextField();
        lblMessage = new javax.swing.JLabel();
        pnlFamilyTree = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbFamilyTree = new javax.swing.JComboBox<>();
        pnlFamilyMembers = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cmbFamilyMember = new javax.swing.JComboBox<>();
        btnAddFamilyMember = new javax.swing.JButton();
        scrlFamilyMembers = new javax.swing.JScrollPane();
        tblFamilyMembers = new javax.swing.JTable();
        lblUloga = new javax.swing.JLabel();
        cmbRelationshipType = new javax.swing.JComboBox<>();
        btnRemoveFamilyMember = new javax.swing.JButton();
        btnAddMember = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Unos novog člana u rodoslov");

        pnlNewMemberData.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Podaci o članu"));

        lblName.setText("Ime člana:");

        lblSurname.setText("Prezime člana:");

        lblGender.setText("Pol:");

        lblBirthDate.setText("Datum rođenja:");

        lblBirthPlace.setText("Mesto rođenja:");

        lblBiography.setText("URL ka biografiji:");

        lblPicture.setText("URL ka slici:");

        grpLivingStatus.add(checkAlive);
        checkAlive.setText("Član je živ");
        checkAlive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkAliveActionPerformed(evt);
            }
        });

        grpLivingStatus.add(checkDead);
        checkDead.setText("Član je preminuo");
        checkDead.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkDeadActionPerformed(evt);
            }
        });

        lblAddress.setText("Trenutna adresa člana:");

        javax.swing.GroupLayout pnlSpecificAliveLayout = new javax.swing.GroupLayout(pnlSpecificAlive);
        pnlSpecificAlive.setLayout(pnlSpecificAliveLayout);
        pnlSpecificAliveLayout.setHorizontalGroup(
                pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlSpecificAliveLayout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(lblAddress)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAddress)
                                .addContainerGap())
        );
        pnlSpecificAliveLayout.setVerticalGroup(
                pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlSpecificAliveLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblAddress)
                                        .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(82, Short.MAX_VALUE))
        );

        lblDeathDate.setText("Datum smrti:");

        lblDeathPlace.setText("Mesto smrti:");

        txtDeathDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        lblDateMessage.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblDateMessage.setText("Datum mora biti u formatu dd/MM/yyyy");

        javax.swing.GroupLayout pnlSpecificDeadLayout = new javax.swing.GroupLayout(pnlSpecificDead);
        pnlSpecificDead.setLayout(pnlSpecificDeadLayout);
        pnlSpecificDeadLayout.setHorizontalGroup(
                pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                                                .addComponent(lblDeathDate)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtDeathDate, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(lblDateMessage)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                                                .addComponent(lblDeathPlace)
                                                .addGap(20, 20, 20)
                                                .addComponent(txtDeathPlace, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        pnlSpecificDeadLayout.setVerticalGroup(
                pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblDeathDate)
                                        .addComponent(txtDeathDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblDateMessage))
                                .addGap(18, 18, 18)
                                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblDeathPlace)
                                        .addComponent(txtDeathPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(45, Short.MAX_VALUE))
        );

        layerSpecificInfo.setLayer(pnlSpecificAlive, javax.swing.JLayeredPane.DEFAULT_LAYER);
        layerSpecificInfo.setLayer(pnlSpecificDead, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layerSpecificInfoLayout = new javax.swing.GroupLayout(layerSpecificInfo);
        layerSpecificInfo.setLayout(layerSpecificInfoLayout);
        layerSpecificInfoLayout.setHorizontalGroup(
                layerSpecificInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layerSpecificInfoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(pnlSpecificAlive, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
                        .addGroup(layerSpecificInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layerSpecificInfoLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(pnlSpecificDead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addContainerGap()))
        );
        layerSpecificInfoLayout.setVerticalGroup(
                layerSpecificInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layerSpecificInfoLayout.createSequentialGroup()
                                .addComponent(pnlSpecificAlive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(layerSpecificInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(pnlSpecificDead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnGroupGender.add(checkMale);
        checkMale.setText("Muški");

        btnGroupGender.add(checkFemale);
        checkFemale.setText("Ženski");

        txtBirthDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        lblMessage.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblMessage.setText("Datum mora biti u formatu: dd/MM/yyyy");

        javax.swing.GroupLayout pnlNewMemberDataLayout = new javax.swing.GroupLayout(pnlNewMemberData);
        pnlNewMemberData.setLayout(pnlNewMemberDataLayout);
        pnlNewMemberDataLayout.setHorizontalGroup(
                pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                                .addGap(124, 124, 124)
                                                .addComponent(checkAlive)
                                                .addGap(158, 158, 158)
                                                .addComponent(checkDead)
                                                .addGap(0, 146, Short.MAX_VALUE))
                                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(layerSpecificInfo))
                                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(lblBiography, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(lblSurname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(lblGender, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(lblBirthDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(lblBirthPlace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(lblPicture, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(txtName)
                                                        .addComponent(txtSurname, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(txtBirthPlace, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(txtUrlBiography, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(txtUrlPicture, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                                                                .addComponent(checkMale)
                                                                                .addGap(18, 18, 18)
                                                                                .addComponent(checkFemale))
                                                                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                                                                .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(lblMessage)))
                                                                .addGap(0, 0, Short.MAX_VALUE)))))
                                .addContainerGap())
        );
        pnlNewMemberDataLayout.setVerticalGroup(
                pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                .addContainerGap(21, Short.MAX_VALUE)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblName)
                                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblSurname)
                                        .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblGender)
                                        .addComponent(checkMale)
                                        .addComponent(checkFemale))
                                .addGap(18, 18, 18)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblBirthDate)
                                        .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblMessage))
                                .addGap(18, 18, 18)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblBirthPlace)
                                        .addComponent(txtBirthPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblBiography)
                                        .addComponent(txtUrlBiography, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblPicture)
                                        .addComponent(txtUrlPicture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(checkAlive)
                                        .addComponent(checkDead))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(layerSpecificInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24))
        );

        jLabel1.setText("Izaberite porodnično stablo za člana:");

        cmbFamilyTree.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFamilyTreeItemStateChanged(evt);
            }
        });
        cmbFamilyTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbFamilyTreeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlFamilyTreeLayout = new javax.swing.GroupLayout(pnlFamilyTree);
        pnlFamilyTree.setLayout(pnlFamilyTreeLayout);
        pnlFamilyTreeLayout.setHorizontalGroup(
                pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlFamilyTreeLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmbFamilyTree, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
        pnlFamilyTreeLayout.setVerticalGroup(
                pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlFamilyTreeLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(cmbFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlFamilyMembers.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Članovi porodice")));

        jLabel2.setText("Odaberite člana uže porodice:");

        btnAddFamilyMember.setText("Dodajte člana porodice");
        btnAddFamilyMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddFamilyMemberActionPerformed(evt);
            }
        });

        scrlFamilyMembers.setViewportView(tblFamilyMembers);

        lblUloga.setText("Dodajte njegovu/njenu ulogu:");

        btnRemoveFamilyMember.setText("Uklonite člana porodice");
        btnRemoveFamilyMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveFamilyMemberActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlFamilyMembersLayout = new javax.swing.GroupLayout(pnlFamilyMembers);
        pnlFamilyMembers.setLayout(pnlFamilyMembersLayout);
        pnlFamilyMembersLayout.setHorizontalGroup(
                pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(scrlFamilyMembers, javax.swing.GroupLayout.DEFAULT_SIZE, 656, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFamilyMembersLayout.createSequentialGroup()
                                                .addComponent(btnRemoveFamilyMember)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(btnAddFamilyMember))
                                        .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                                                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(lblUloga, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(cmbFamilyMember, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(cmbRelationshipType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addContainerGap())
        );
        pnlFamilyMembersLayout.setVerticalGroup(
                pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(cmbFamilyMember, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblUloga)
                                        .addComponent(cmbRelationshipType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnAddFamilyMember)
                                        .addComponent(btnRemoveFamilyMember))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(scrlFamilyMembers, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        btnAddMember.setText("Dodajte člana");
        btnAddMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMemberActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(pnlFamilyMembers, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(pnlFamilyTree, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(pnlNewMemberData, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(btnAddMember)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(pnlNewMemberData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pnlFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pnlFamilyMembers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAddMember)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void checkDeadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkDeadActionPerformed
        layerSpecificInfo.setVisible(true);
        pnlSpecificAlive.setVisible(false);
        pnlSpecificDead.setVisible(true);
    }//GEN-LAST:event_checkDeadActionPerformed

    private void checkAliveActionPerformed(java.awt.event.ActionEvent evt) {
        layerSpecificInfo.setVisible(true);
        pnlSpecificAlive.setVisible(true);
        pnlSpecificDead.setVisible(false);

    }

    private void cmbFamilyTreeActionPerformed(java.awt.event.ActionEvent evt) {
//        /*Pri odabiru stabla najpre treba da kreiramo objekat Membership koji ćemo proslediti modelu tabele koja sadrži srodstva tj. članove porodice*/
//
//        /*kreiranje Membership*/
//        Gender gender = checkMale.isSelected() ? Gender.MALE : Gender.FEMALE;
//
//        String name = txtName.getText().trim();
//        String surname = txtSurname.getText().trim();
//        String birthPlace = txtBirthPlace.getText().trim();
//        String deathPlace = txtDeathPlace.getText().trim();
//        String address = txtAddress.getText().trim();
//        String biographyURL = txtUrlBiography.getText().trim();
//        String pictureURL = txtUrlPicture.getText().trim();
//
//        Date birthDate = (Date) txtBirthDate.getValue();
//        Date deathDate = (Date) txtDeathDate.getValue();
//
//        this.newMembership = controller.createMembership(name, surname, gender, birthDate, deathDate,
//                address, birthPlace, deathPlace, biographyURL, pictureURL,
//                (FamilyTree) cmbFamilyTree.getSelectedItem(), user);
//        /*kreiranje Membership*/
//
//        try {
//            Member[] members = controller.getMembersOfTree((FamilyTree) cmbFamilyTree.getSelectedItem());
//            if (members.length != 0) {
//                pnlFamilyMembers.setVisible(true);
//                cmbFamilyMember.setModel(new DefaultComboBoxModel<>(members));
//                this.model = new FamilyMembersTableModel(this.newMembership);
//                tblFamilyMembers.setModel(this.model);
//            } else System.out.println("Nema clanova u izabranom stablu");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private void btnAddFamilyMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddFamilyMemberActionPerformed
        FamilyMembersTableModel model = (FamilyMembersTableModel) tblFamilyMembers.getModel();

        model.addItem(controller.createFamilyRelationship(this.newMembership, model.getRelationships().size(), (Member) cmbFamilyMember.getSelectedItem(), (RelationshipType) cmbRelationshipType.getSelectedItem()));
    }//GEN-LAST:event_btnAddFamilyMemberActionPerformed

    private void btnAddMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMemberActionPerformed


        if (this.newMembership != null) {
            try {
                Membership newMembership = controller.addMember(this.newMembership, this.model);
//                Membership newMembership = controller.addMember(this.newMembership, (FamilyMembersTableModel) tblFamilyMembers.getModel());
                JOptionPane.showMessageDialog(this, newMembership.toString(), "Uspesno sacuvan clan", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            JOptionPane.showMessageDialog(this, "Morate odabrati stablo u koje će novi član biti unet", "Greška", JOptionPane.ERROR_MESSAGE);
    }//GEN-LAST:event_btnAddMemberActionPerformed

    private void btnRemoveFamilyMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveFamilyMemberActionPerformed
        FamilyMembersTableModel model = (FamilyMembersTableModel) tblFamilyMembers.getModel();
        model.removeItem(tblFamilyMembers.getSelectedRow());
    }//GEN-LAST:event_btnRemoveFamilyMemberActionPerformed


    private void cmbFamilyTreeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFamilyTreeItemStateChanged

        if (evt.getStateChange() == ItemEvent.SELECTED) {
            /*Pri odabiru stabla najpre treba da kreiramo objekat Membership koji ćemo proslediti modelu tabele koja sadrži srodstva tj. članove porodice*/

            /*kreiranje Membership*/
            Gender gender = checkMale.isSelected() ? Gender.MALE : Gender.FEMALE;

            String name = txtName.getText().trim();
            String surname = txtSurname.getText().trim();
            String birthPlace = txtBirthPlace.getText().trim();
            String deathPlace = txtDeathPlace.getText().trim();
            String address = txtAddress.getText().trim();
            String biographyURL = txtUrlBiography.getText().trim();
            String pictureURL = txtUrlPicture.getText().trim();

            Date birthDate = (Date) txtBirthDate.getValue();
            Date deathDate = (Date) txtDeathDate.getValue();

            this.newMembership = controller.createMembership(name, surname, gender, birthDate, deathDate,
                    address, birthPlace, deathPlace, biographyURL, pictureURL,
                    (FamilyTree) cmbFamilyTree.getSelectedItem(), user);
            /*kreiranje Membership*/

            try {
                Member[] members = controller.getMembersOfTree((FamilyTree) cmbFamilyTree.getSelectedItem());
                if (members.length != 0) {
                    pnlFamilyMembers.setVisible(true);
                    cmbFamilyMember.setModel(new DefaultComboBoxModel<>(members));
                    this.model = new FamilyMembersTableModel(this.newMembership);
                    tblFamilyMembers.setModel(this.model);
                } else System.out.println("Nema clanova u izabranom stablu");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }//GEN-LAST:event_cmbFamilyTreeItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddFamilyMember;
    private javax.swing.JButton btnAddMember;
    private javax.swing.ButtonGroup btnGroupGender;
    private javax.swing.JButton btnRemoveFamilyMember;
    private javax.swing.JCheckBox checkAlive;
    private javax.swing.JCheckBox checkDead;
    private javax.swing.JCheckBox checkFemale;
    private javax.swing.JCheckBox checkMale;
    private javax.swing.JComboBox<Member> cmbFamilyMember;
    private javax.swing.JComboBox<FamilyTree> cmbFamilyTree;
    private javax.swing.JComboBox<RelationshipType> cmbRelationshipType;
    private javax.swing.ButtonGroup grpLivingStatus;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLayeredPane layerSpecificInfo;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblBiography;
    private javax.swing.JLabel lblBirthDate;
    private javax.swing.JLabel lblBirthPlace;
    private javax.swing.JLabel lblDateMessage;
    private javax.swing.JLabel lblDeathDate;
    private javax.swing.JLabel lblDeathPlace;
    private javax.swing.JLabel lblGender;
    private javax.swing.JLabel lblMessage;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPicture;
    private javax.swing.JLabel lblSurname;
    private javax.swing.JLabel lblUloga;
    private javax.swing.JPanel pnlFamilyMembers;
    private javax.swing.JPanel pnlFamilyTree;
    private javax.swing.JPanel pnlNewMemberData;
    private javax.swing.JPanel pnlSpecificAlive;
    private javax.swing.JPanel pnlSpecificDead;
    private javax.swing.JScrollPane scrlFamilyMembers;
    private javax.swing.JTable tblFamilyMembers;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JFormattedTextField txtBirthDate;
    private javax.swing.JTextField txtBirthPlace;
    private javax.swing.JFormattedTextField txtDeathDate;
    private javax.swing.JTextField txtDeathPlace;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtSurname;
    private javax.swing.JTextField txtUrlBiography;
    private javax.swing.JTextField txtUrlPicture;
    // End of variables declaration//GEN-END:variables

    private void prepareComponents() {

//        tblFamilyMembers.setModel(new FamilyMembersTableModel()); // TODO: moora da se lepo podesi model tabele

        checkMale.setSelected(true);
        layerSpecificInfo.setVisible(false);
        pnlFamilyMembers.setVisible(false);

        try {
            cmbFamilyTree.setModel(new DefaultComboBoxModel<>(controller.getExistingFamilyTrees()));
            cmbFamilyTree.setSelectedIndex(-1);
//            cmbFamilyTree.addItemListener(this::cmbFamilyTreeItemStateChanged);
//            cmbFamilyTree.addActionListener(this::cmbFamilyTreeActionPerformed);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            cmbRelationshipType.setModel(new DefaultComboBoxModel<>(controller.getRelationshipTypes()));
            cmbRelationshipType.setSelectedIndex(-1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
