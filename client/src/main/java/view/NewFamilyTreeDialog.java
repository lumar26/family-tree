package view;

import controller.Controller;
import model.FamilyTree;
import model.User;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class NewFamilyTreeDialog extends javax.swing.JDialog {

    private Controller controller;
    private User currentUser;

    public NewFamilyTreeDialog(Frame parent, boolean modal, User currentUser) {
        super(parent, modal);
        this.currentUser = currentUser;
        try {
            controller = Controller.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initComponents();
        prepareDialog();

    }

    private void prepareDialog() {
        pnlInsertInfo.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDialogDescription = new javax.swing.JLabel();
        btnCreateNewFamilyTree = new javax.swing.JButton();
        pnlInsertInfo = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblDescription = new javax.swing.JLabel();
        lblStartDate = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtDescription = new javax.swing.JTextField();
        dateStartDate = new javax.swing.JFormattedTextField();
        lblInform = new javax.swing.JLabel();
        btnSaveFamilyTree = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Novi rodoslov");

        lblDialogDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDialogDescription.setText("Unos novog rodoslova u sistem");

        btnCreateNewFamilyTree.setText("Kreiraj novi rodoslov");
        btnCreateNewFamilyTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateNewFamilyTreeActionPerformed(evt);
            }
        });

        pnlInsertInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Unos opštih informacija o rodoslovu"));

        lblName.setText("Naziv vašeg rodoslova:");

        lblDescription.setText("Kratak opis Vašeg rodoslova:");

        lblStartDate.setText("Datum početka posmatranja rodoslova:");

        dateStartDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));
        dateStartDate.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        lblInform.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblInform.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInform.setText(" format dd/MM/yyyy je obavezan");

        javax.swing.GroupLayout pnlInsertInfoLayout = new javax.swing.GroupLayout(pnlInsertInfo);
        pnlInsertInfo.setLayout(pnlInsertInfoLayout);
        pnlInsertInfoLayout.setHorizontalGroup(
            pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtName)
                    .addComponent(txtDescription)
                    .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                        .addComponent(dateStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblInform, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlInsertInfoLayout.setVerticalGroup(
            pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                        .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblName)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(50, 50, 50)
                        .addComponent(lblDescription))
                    .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStartDate)
                    .addComponent(dateStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblInform))
                .addGap(46, 46, 46))
        );

        btnSaveFamilyTree.setText("Sačuvaj rodoslov");
        btnSaveFamilyTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveFamilyTreeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblDialogDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(btnCreateNewFamilyTree)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnlInsertInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSaveFamilyTree)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDialogDescription)
                .addGap(18, 18, 18)
                .addComponent(btnCreateNewFamilyTree)
                .addGap(18, 18, 18)
                .addComponent(pnlInsertInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSaveFamilyTree)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateNewFamilyTreeActionPerformed(java.awt.event.ActionEvent evt) {                                                       
        pnlInsertInfo.setVisible(true);
//        controller.createNewFamilyTree();
    }

    private void btnSaveFamilyTreeActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            FamilyTree familyTree = controller.saveNewFamilyTree(txtName.getText(), txtDescription.getText(), dateStartDate.getValue(), currentUser);
            JOptionPane.showMessageDialog(this, "Sačuvan rodoslov: " + familyTree.toString());
            dispose();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Nije moguće uspostaviti vezu sa serverom", "Greška", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Nije moguće dato stablo sačuvati", "Greška", JOptionPane.ERROR_MESSAGE);

        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateNewFamilyTree;
    private javax.swing.JButton btnSaveFamilyTree;
    private javax.swing.JFormattedTextField dateStartDate;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblDialogDescription;
    private javax.swing.JLabel lblInform;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblStartDate;
    private javax.swing.JPanel pnlInsertInfo;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
