/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import model.User;

import java.io.IOException;

public class MainForm extends javax.swing.JFrame {

    private User currentUser;

    public MainForm(User user) {
        this.currentUser = user;
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainMenuBar = new javax.swing.JMenuBar();
        menuFamilyTree = new javax.swing.JMenu();
        itemCreateFamilyTree = new javax.swing.JMenuItem();
        menuDeleteFamilyTree = new javax.swing.JMenuItem();
        menuMember = new javax.swing.JMenu();
        itmNewMember = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Vaše porodično stablo");

        menuFamilyTree.setText("Rodoslov");
        menuFamilyTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuFamilyTreeActionPerformed(evt);
            }
        });

        itemCreateFamilyTree.setText("Kreiraj novi rodoslov");
        itemCreateFamilyTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCreateFamilyTreeActionPerformed(evt);
            }
        });
        menuFamilyTree.add(itemCreateFamilyTree);

        menuDeleteFamilyTree.setText("Obriši rodoslov");
        menuFamilyTree.add(menuDeleteFamilyTree);

        mainMenuBar.add(menuFamilyTree);

        menuMember.setText("Član");

        itmNewMember.setText("Unos novog člana u rodoslov");
        itmNewMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmNewMemberActionPerformed(evt);
            }
        });
        menuMember.add(itmNewMember);

        mainMenuBar.add(menuMember);

        setJMenuBar(mainMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1040, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 181, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuFamilyTreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuFamilyTreeActionPerformed


    }//GEN-LAST:event_menuFamilyTreeActionPerformed

    private void itemCreateFamilyTreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCreateFamilyTreeActionPerformed

        (new NewFamilyTreeDialog(this, true, currentUser)).setVisible(true);

    }//GEN-LAST:event_itemCreateFamilyTreeActionPerformed

    private void itmNewMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmNewMemberActionPerformed

        (new NewMemberForm(this, true, currentUser)).setVisible(true);

    }//GEN-LAST:event_itmNewMemberActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem itemCreateFamilyTree;
    private javax.swing.JMenuItem itmNewMember;
    private javax.swing.JMenuBar mainMenuBar;
    private javax.swing.JMenuItem menuDeleteFamilyTree;
    private javax.swing.JMenu menuFamilyTree;
    private javax.swing.JMenu menuMember;
    // End of variables declaration//GEN-END:variables


    @Override
    public void dispose() {

        try {
            Controller.getInstance().logout();
        } catch (IOException e) {
            e.printStackTrace();
        }

        super.dispose();
        System.exit(0);
    }


}
