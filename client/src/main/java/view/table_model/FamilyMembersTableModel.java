package view.table_model;

import model.FamilyRelationship;
import model.Membership;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class FamilyMembersTableModel extends AbstractTableModel {

    private Membership membership;
    private String[] columnNames = new String[]{"Član porodice", "Srodstvo"};

    public FamilyMembersTableModel(Membership membership) {
        this.membership = membership;
    }

    @Override
    public int getRowCount() {
        return membership.getRelationships().size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int row, int col) {
        FamilyRelationship item = membership.getRelationships().get(row);
        switch (col) {
            case 0:
                return item.getFamilyMember().toString();
            case 1:
                return item.getRelationshipType().toString();
            default:
                return "nije naznačeno";
        }
    }

    public List<FamilyRelationship> getRelationships() {
        return membership.getRelationships();
    }

    public void addItem(FamilyRelationship familyRelationship){
        membership.getRelationships().add(familyRelationship);
        int insertedRow = membership.getRelationships().size() -1;
        fireTableRowsInserted(insertedRow, insertedRow);
    }

    public void removeItem(int row){
        membership.getRelationships().remove(row);
        fireTableRowsDeleted(row, row);
    }
}
