package controller;

import exception.UserNotFoundException;
import model.*;
import model.enums.Gender;
import network.Operation;
import network.Request;
import network.Response;
import network.ResponseStatus;
import util.PropertiesCache;
import view.table_model.FamilyMembersTableModel;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Controller {
    private static Controller instance;
    private final Socket socket;
    //    private final PrintStream toServer; // s obzirom da hocu da se sve salje kao JSON onda cemo ovako
//    private final BufferedReader fromServer;
    private final ObjectOutputStream toServer;
    private final ObjectInputStream fromServer;
    private final PropertiesCache props;

    private Controller() throws IOException {
        props = PropertiesCache.getInstance();
        socket = new Socket(props.getProperty("server_name"), Integer.parseInt(props.getProperty("server_port")));
//        toServer = new PrintStream(socket.getOutputStream());
//        fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        toServer = new ObjectOutputStream(socket.getOutputStream());
        fromServer = new ObjectInputStream((socket.getInputStream()));

    }

    public static Controller getInstance() throws IOException {
        if (instance == null)
            instance = new Controller();
        return instance;
    }

    public User login(String username, String password) throws UserNotFoundException {
        Map<String, String> loginMap = new HashMap<>();
        loginMap.put("username", username);
        loginMap.put("password", password);

//        LoginMap sendData = new LoginMap(loginMap);
//        toServer.println(JsonConverter.toJson((new Request(sendData, Operation.LOGIN))));
        try {
            toServer.writeObject(new Request(loginMap, Operation.LOGIN));
            toServer.flush();
        } catch (IOException e) {
            System.err.println("Error while sending request");
            e.printStackTrace();
        }
//        waiting for response from server

        try {
//            Response response = JsonConverter.toObject(fromServer.readLine(), Response.class);
            Response response = (Response) fromServer.readObject();
            if (response.getStatus().equals(ResponseStatus.ERROR)) {
                throw (UserNotFoundException) response.getError();
            }
            return (User) response.getData();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new UserNotFoundException("Login failed"); // TODO: change this to something more meaningful


    }

    public FamilyTree saveNewFamilyTree(String name, String description, Object startDate, User currentUser) throws Exception {
        Date temp = (Date) startDate;
        LocalDate date = temp.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        FamilyTree tree = new FamilyTree(name, description, date, currentUser);

        Request saveTreeRequest = new Request(tree, Operation.SAVE_FAMILY_TREE);
        try {
            toServer.writeObject(saveTreeRequest);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

        try {
            Response response = (Response) fromServer.readObject();
            if (!response.getStatus().equals(ResponseStatus.ERROR))
                return (FamilyTree) response.getData();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new Exception("Nije moguće sačuvati novo porodično stablo");
    }

    public FamilyTree[] getExistingFamilyTrees() throws Exception {
        FamilyTree[] result;
        Request req = new Request(null, Operation.GET_EXISTING_TREES);
        try {
            toServer.writeObject(req);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

        try {
            Response response = (Response) fromServer.readObject();
            if (!response.getStatus().equals(ResponseStatus.ERROR))
                return (FamilyTree[]) response.getData();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new Exception("Nije moguće dobiti porodična stabla od servera");
    }

    public Member[] getMembersOfTree(FamilyTree selectedTree) throws Exception {
        Member[] result;
        Request req = new Request(selectedTree, Operation.GET_MEMBERS_OF_TREE);
        try {
            toServer.writeObject(req);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

        try {
            Response response = (Response) fromServer.readObject();
            if (!response.getStatus().equals(ResponseStatus.ERROR))
                return (Member[]) response.getData();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new Exception("Nije moguće dobiti porodična stabla od servera");
    }

//    public Membership addMember(String name, String surname, Gender g, Date birthDate, Date deathDate,
//                            String address, String birthPlace, String deathPlace, String biographyURL, String pictureURL,
//                            FamilyTree familyTree, User user, FamilyMembersTableModel tableModel) throws Exception {
//        LocalDate birthDateLD =  birthDate.toInstant()
//                .atZone(ZoneId.systemDefault())
//                .toLocalDate();
//        LocalDate deathDateLD = null;
//        if (deathDate != null){
//            deathDateLD = deathDate.toInstant()
//                    .atZone(ZoneId.systemDefault())
//                    .toLocalDate();
//        }
////        List<Member> familyMembers = tableModel.
//
//        Member member = new Member(name, surname, birthDateLD, deathDateLD, g, birthPlace, deathPlace, address, biographyURL, pictureURL);
//        Map<String, Object> requestData = new HashMap<>();
//        requestData.put("member", member);
//        requestData.put("familyTree", familyTree);
//        requestData.put("user", user);
//        requestData.put("familyMembers", null); // TODO: da se prosledjuju i clanovi porodice
//
//        Request req = new Request(requestData, Operation.ADD_NEW_MEMBER);
//        try {
//            toServer.writeObject(req);
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw e;
//        }
//
//
//        try {
//            Response res = (Response) fromServer.readObject();
//            if (!res.getStatus().equals(ResponseStatus.ERROR))
//                return (Membership) res.getData();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        throw new Exception("Could not save new Member of Family Tree");
//    }


    /*Kreiranje članstva bez reference na članove porodice, tj. srodstva*/
    public Membership createMembership(String name, String surname, Gender gender, Date birthDate,
                                       Date deathDate, String address, String birthPlace, String deathPlace,
                                       String biographyURL, String pictureURL, FamilyTree selectedFamilyTree, User user) {
        System.out.println(birthDate);
        LocalDate birthDateLD = birthDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        LocalDate deathDateLD = null;
        if (deathDate != null) {
            deathDateLD = deathDate.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        }

        Member member = new Member(name, surname, birthDateLD, deathDateLD, gender, birthPlace, deathPlace, address, biographyURL, pictureURL);

        Membership result = new Membership(LocalDate.now(), selectedFamilyTree, user, member);
        return result;
    }

    public Membership addMember(Membership newMembership, FamilyMembersTableModel model) throws Exception {
        Map<String, Object> requestData = new HashMap<>();
        requestData.put("membership", newMembership);
        requestData.put("familyMembers", model == null ? new ArrayList<FamilyRelationship>() : model.getRelationships());

        Request req = new Request(requestData, Operation.ADD_NEW_MEMBER);
        try {
            toServer.writeObject(req);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }


        try {
            Response res = (Response) fromServer.readObject();
            if (!res.getStatus().equals(ResponseStatus.ERROR))
                return (Membership) res.getData();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        throw new Exception("Could not save new Member of Family Tree");
    }

    public RelationshipType[] getRelationshipTypes() throws Exception {
        RelationshipType[] result;
        Request req = new Request(null, Operation.GET_RELATIONSHIP_TYPES);
        try {
            toServer.writeObject(req);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

        try {
            Response response = (Response) fromServer.readObject();
            if (!response.getStatus().equals(ResponseStatus.ERROR))
                return (RelationshipType[]) response.getData();
            else System.out.println(response.getError());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new Exception("Nije moguće dobiti porodična stabla od servera");
    }

    public FamilyRelationship createFamilyRelationship(Membership newMembership, int size, Member member, RelationshipType type) {
        return new FamilyRelationship(newMembership, size, member, type);
    }

    public void logout() throws IOException {
        toServer.writeObject(new Request(null, Operation.LOGOUT));
        System.out.println("Odjava...");
    }
}
