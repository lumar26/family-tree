package repository;

import model.User;
import repository.impl.UserRepository;

import java.sql.SQLException;
import java.util.List;

public class UserRepositoryTest {
    public static void main(String[] args) throws SQLException {
        User u1 = new User("lumar", "lumar", "lumar@gmail.com", "Luka", "Marinković");
        User u2 = new User("lamar", "lamar", "lamar@gmail.com", "Lazar", "Marinković");
        User u3 = new User("vasa", "vasa", "vasa@gmail.com", "Vasilije", "Mijušković");
        User u4 = new User("mare", "mare", "mare@gmail.com", "Marko", "Milenković");

        Repository userRepository = new UserRepository();
        try {
            System.out.println("Unose se korisnici: \n");
            System.out.println(userRepository.save(u1).toString());
            System.out.println(userRepository.save(u2).toString());
            System.out.println(userRepository.save(u3).toString());
            userRepository.commit();

            System.out.println("Korisnici uneseni u bazu:\n");
            for (User u: (List<User>)userRepository.findAll()){
                System.out.println(u.toString());
            }


            System.out.println("Updating user u3:\n");
            userRepository.update(u4, u3.getId());
            userRepository.commit();

            System.out.println("Deleting: " + userRepository.delete(6L).toString());
            userRepository.commit();


        } catch (SQLException e) {
            userRepository.rollback();
            e.printStackTrace();
        }
    }
}
