package repository;

import model.FamilyTree;
import model.User;
import repository.impl.FamilyTreeRepository;
import repository.impl.UserRepository;

import java.sql.SQLException;
import java.time.LocalDate;

public class FamilyTreeRepositoryTest {
    public static void main(String[] args) throws SQLException {

        Repository<FamilyTree, Long> familyTreeRepository = new FamilyTreeRepository();
        Repository<User, Long> userRepository = new UserRepository();

        FamilyTree ft1 = new FamilyTree("Marinkovići",
                "Rodoslov Marinkovića iz Krežbinca",
                LocalDate.of(1800, 1,1), userRepository.findByID(5L));

        FamilyTree ft2 = new FamilyTree("Jaćimovići",
                "Rodoslov Jaćimovića iz Krežbinca",
                LocalDate.of(1850, 1,1), userRepository.findByID(7L));

        FamilyTree ft3 = new FamilyTree("Jankovići",
                "Rodoslov Jankovića iz Krežbinca",
                LocalDate.of(1850, 1,1), userRepository.findByID(7L));


        try {
            System.out.println("Unose se korisnici: \n");
            System.out.println(familyTreeRepository.save(ft1).toString());
            System.out.println(familyTreeRepository.save(ft2).toString());
            userRepository.commit();

            System.out.println("Korisnici uneseni u bazu:\n");
            for (FamilyTree u: familyTreeRepository.findAll()){
                System.out.println(u.toString());
            }


            System.out.println("Izmena rodoslova ft3:\n");
            System.out.println(familyTreeRepository.update(ft3, ft2.getId()));
            familyTreeRepository.commit();

//            System.out.println("Deleting: " + familyTreeRepository.delete(6L).toString());
//            familyTreeRepository.commit();


        } catch (SQLException e) {
            userRepository.rollback();
            e.printStackTrace();
        }
    }
}
