package service;

import exception.UserNotFoundException;
import model.User;

public interface UserService {
    User login(String username, String password) throws UserNotFoundException;
}
