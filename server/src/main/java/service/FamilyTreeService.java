package service;

import exception.StructuralBoundaryException;
import model.FamilyTree;

import java.sql.SQLException;

public interface FamilyTreeService {
    FamilyTree saveNewFamilyTree(FamilyTree familyTree) throws SQLException, StructuralBoundaryException;

    FamilyTree[] getTrees() throws SQLException;
}
