package service;

import model.FamilyRelationship;
import model.FamilyTree;
import model.Member;
import model.Membership;

import java.sql.SQLException;
import java.util.List;

public interface MembershipService {
    Member[] getMembersOf(FamilyTree familyTree) throws SQLException;

    Membership addMembership(Membership membership, List<FamilyRelationship> familyRelationships) throws Exception;
}
