package service;

import model.RelationshipType;

import java.sql.SQLException;

public interface RelationshipTypeService {
    RelationshipType[] getCloseFamilyRoles() throws SQLException;
}
