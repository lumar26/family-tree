package service;

import exception.StructuralBoundaryException;
import model.FamilyRelationship;

import java.sql.SQLException;
import java.util.List;

public interface FamilyRelationshipService {
    List<FamilyRelationship> saveAllRelationships(List<FamilyRelationship> familyRelationships) throws SQLException, StructuralBoundaryException;
}
