package service.impl;

import exception.StructuralBoundaryException;
import exception.enums.BoundaryType;
import exception.enums.StructuralOperation;
import model.FamilyTree;
import model.User;
import repository.Repository;
import repository.impl.FamilyTreeRepository;
import repository.impl.UserRepository;
import service.FamilyTreeService;

import java.sql.SQLException;
import java.util.List;

public class FamilyTreeServiceImpl implements FamilyTreeService {

    private Repository<FamilyTree, Long> familyTreeRepository;
    private Repository<User, Long> userRepository;

    public FamilyTreeServiceImpl() {
        this.userRepository = new UserRepository();
        this.familyTreeRepository = new FamilyTreeRepository();
    }

    @Override
    public FamilyTree saveNewFamilyTree(FamilyTree familyTree) throws SQLException, StructuralBoundaryException {

        /*provera strukturnog ograničenja INSERT RESTRICTED User*/
        try {
            userRepository.findByID(familyTree.getUser().getId());
        } catch (SQLException e) {

            throw new StructuralBoundaryException("Rodoslov", BoundaryType.RESTRICTED, StructuralOperation.INSERT);
        }
        FamilyTree res = familyTreeRepository.save(familyTree);
        familyTreeRepository.commit();
        return res;
    }

    @Override
    public FamilyTree[] getTrees() throws SQLException {
        List<FamilyTree> trees = familyTreeRepository.findAll();
        return trees.toArray(FamilyTree[]::new);
    }
}
