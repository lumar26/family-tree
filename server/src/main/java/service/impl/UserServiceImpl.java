package service.impl;

import exception.UserNotFoundException;
import model.User;
import repository.Repository;
import repository.impl.UserRepository;
import service.UserService;

import java.sql.SQLException;
import java.util.List;

public class UserServiceImpl implements UserService {

    private Repository<User, Long> userRepo;

    public UserServiceImpl() {
        this.userRepo = new UserRepository();
    }

    @Override
    public User login(String username, String password) throws UserNotFoundException {
        try {
            List<User> users = userRepo.findAll();
            for (User u : users){
                if (u.getUsername().equals(username) && u.getPassword().equals(password)){
                    return u;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new UserNotFoundException("There is no user for given username and password");
    }
}
