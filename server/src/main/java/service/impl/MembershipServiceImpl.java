package service.impl;

import exception.StructuralBoundaryException;
import exception.enums.BoundaryType;
import exception.enums.StructuralOperation;
import model.*;
import repository.Repository;
import repository.impl.FamilyTreeRepository;
import repository.impl.MemberRepository;
import repository.impl.MembershipRepository;
import repository.impl.UserRepository;
import service.FamilyRelationshipService;
import service.MembershipService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MembershipServiceImpl implements MembershipService {
    Repository<Membership, Long> membershipRepository = new MembershipRepository();
    Repository<Member, Long> memberRepository = new MemberRepository();
    Repository<FamilyTree, Long> familyTreeRepository = new FamilyTreeRepository();
    Repository<User, Long> userRepository = new UserRepository();

    FamilyRelationshipService relationshipService = new FamilyRelationshipServiceImpl();

    @Override
    public Member[] getMembersOf(FamilyTree familyTree) throws SQLException {
        List<Member> members = new ArrayList<>();
        List<Membership> memberships = membershipRepository.findAll();
        for (Membership ms : memberships) {
            if (ms.getFamilyTree().equals(familyTree)) members.add(ms.getMember());
        }
        return members.toArray(Member[]::new);
    }

    @Override
    public Membership addMembership(Membership membership, List<FamilyRelationship> familyRelationships) throws Exception {
        /*najpre čuvamo člana u bazu jer sigurno ne postoji jer ne postoji drugi način unosa člana sem preko članstva*/
        memberRepository.save(membership.getMember());
        /*član sačuvan*/

        /*provera uslova da li postoje korisnik, stablo i član za članstvo koje hoćemo da unesemo*/
        try {
            checkAddCondition(membership);
        } catch (StructuralBoundaryException e) {
            membershipRepository.rollback();
            e.printStackTrace();
            throw e;
        }

        /*najpre čuvamo u tabelu membership članstvo*/
        Membership result = membershipRepository.save(membership);

        /*nakon unosa članstva u bazu moramo da unesemo i srodstva kako bi član bio povezan, preko članstva, sa ostalim ččlanovima rodoslova*/
        try {
            List<FamilyRelationship> savedRelationships = relationshipService.saveAllRelationships(familyRelationships);
            result.setRelationships(savedRelationships);
            membershipRepository.commit();
            return result;
        } catch (SQLException | StructuralBoundaryException e) {
            membershipRepository.rollback();
            throw new Exception("Greška pri čuvanju članova porodice za člana (Membership id = " + result.getId() + ") koji je prosleđen");
            //TODO: mora ovo malo smislenije da se uradi, da se iskoristi ovaj StructuralBoundaryException
        }


        /*if (savedRelationships != null) {*//*uspešno su sačuvani svi članovi porodice zajedno sa članstvom*//*
            result.setRelationships(savedRelationships);
            membershipRepository.commit();
            return result;
        } else {
            membershipRepository.rollback();
            throw new Exception("Greška pri čuvanju članova porodice za člana (Membership id = " + result.getId() + ") koji je prosleđen");
        }*/
    }

//    @Override
//    public Membership addMembership(Member member, FamilyTree familyTree, User user) throws SQLException {
//        if (memberRepository.findByID(member.getId()) == null){
//            memberRepository.save(member);// TODO: najpre treba da se sacuva clan posebno, ukoliko vec ne postoji, a ne bi smeo da postoji, ali neka je provera za sad
//        }
//        Membership membership = new Membership(LocalDate.now(), familyTree, user, member);
//        try {
//            checkAddCondition(membership); // ukoliko nisu ispunjeni uslovi desice se izuzetak i nece da dodje do sl. naredbe
//        } catch (Exception e) {
//            membershipRepository.rollback();
//            e.printStackTrace();
//        }
//        Membership result = membershipRepository.save(membership);
//        membershipRepository.commit();
//        return result;
//    }

    private void checkAddCondition(Membership membership) throws Exception {
        try {
            userRepository.findByID(membership.getUser().getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StructuralBoundaryException("Članstvo (nije pronađen korisnik id=" + membership.getUser().getId() + ")", BoundaryType.RESTRICTED, StructuralOperation.INSERT);
        }
        try {
            memberRepository.findByID(membership.getMember().getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StructuralBoundaryException("Članstvo (nije pronađen član id=" + membership.getMember().getId() + ")", BoundaryType.RESTRICTED, StructuralOperation.INSERT);
        }
        try {
            familyTreeRepository.findByID(membership.getFamilyTree().getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StructuralBoundaryException("Članstvo (nije pronađen rodoslov id=" + membership.getFamilyTree().getId() + ")", BoundaryType.RESTRICTED, StructuralOperation.INSERT);
        }
//        TODO: trebalo bi da servisi pristupaju servisima mozda
    }
}
