package service.impl;

import exception.StructuralBoundaryException;
import exception.enums.BoundaryType;
import exception.enums.StructuralOperation;
import model.FamilyRelationship;
import model.Member;
import model.Membership;
import repository.Repository;
import repository.impl.FamilyRelationshipRepository;
import repository.impl.MemberRepository;
import repository.impl.MembershipRepository;
import repository.key.FamilyRelationshipPrimaryKey;
import service.FamilyRelationshipService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FamilyRelationshipServiceImpl implements FamilyRelationshipService {

    Repository<FamilyRelationship, FamilyRelationshipPrimaryKey> familyRelationshipRepository = new FamilyRelationshipRepository();
    Repository<Membership, Long> membershipRepository = new MembershipRepository();
    Repository<Member, Long> memberRepository = new MemberRepository();

    @Override
    public List<FamilyRelationship> saveAllRelationships(List<FamilyRelationship> familyRelationships) throws StructuralBoundaryException, SQLException {
        List<FamilyRelationship> saved = new ArrayList<>();
//        try {
        for (FamilyRelationship element : familyRelationships) {


            /*provera strukturnih ograničenja*/
            try {
                membershipRepository.findByID(element.getMembership().getId());
                memberRepository.findByID(element.getFamilyMember().getId());
            } catch (SQLException ex) {
                System.err.println("Nije pronađeno članstvo (Membership id = " + element.getMembership().getId() + ") za dato srodstvo (FamilyRelationship number = " + element.getNumber() + ")");
                ex.printStackTrace();
                throw new StructuralBoundaryException("Srodstvo", BoundaryType.RESTRICTED, StructuralOperation.INSERT);
            }

            FamilyRelationship savedOne = familyRelationshipRepository.save(element);
            saved.add(savedOne);
        }
        return saved;
//        } catch (SQLException e) {
//            System.err.println("Greška pri čuvanju liste porodičnih članova u bazu");
//            e.printStackTrace();
//            return null;
//        }

    }
}
