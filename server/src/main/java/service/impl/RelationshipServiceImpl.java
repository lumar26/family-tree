package service.impl;

import model.RelationshipType;
import repository.Repository;
import repository.impl.RelationshipTypeRepository;
import service.RelationshipTypeService;

import java.sql.SQLException;
import java.util.List;

public class RelationshipServiceImpl implements RelationshipTypeService {

    Repository<RelationshipType, Long> relationshipTypeRepository = new RelationshipTypeRepository();

    @Override
    public RelationshipType[] getCloseFamilyRoles() throws SQLException {
        List<RelationshipType> types = relationshipTypeRepository.findAll();
        return types.toArray(RelationshipType[]::new);
    }
}
