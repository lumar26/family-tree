package server;

import exception.StructuralBoundaryException;
import exception.UserNotFoundException;
import model.*;
import network.Operation;
import network.Request;
import network.Response;
import network.ResponseStatus;
import service.FamilyTreeService;
import service.MembershipService;
import service.RelationshipTypeService;
import service.UserService;
import service.impl.FamilyTreeServiceImpl;
import service.impl.MembershipServiceImpl;
import service.impl.RelationshipServiceImpl;
import service.impl.UserServiceImpl;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class ClientHandler extends Thread {
    private final Socket socket;
//    private final BufferedReader fromClient;
//    private final PrintStream toClient;

    private final ObjectOutputStream toClient;
    private final ObjectInputStream fromClient;


    UserService userService = new UserServiceImpl();
    FamilyTreeService familyTreeService = new FamilyTreeServiceImpl();
    MembershipService membershipService = new MembershipServiceImpl();
    RelationshipTypeService relationshipTypeService = new RelationshipServiceImpl();


    private User loginUser;

    public ClientHandler(Socket socket) throws IOException {
        this.socket = socket;
        toClient = new ObjectOutputStream(socket.getOutputStream());
        fromClient = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
//                Request requestObject = JsonConverter.toObject(fromClient.readLine(), Request.class);
                Request req = (Request) fromClient.readObject();
                Response response = handleRequest(req);
//                toClient.println(JsonConverter.toJson(response));
                if (response != null) /*u slučaju na primer odjave sa aplikacije ne vraća se nikakav response*/
                toClient.writeObject(response);
            } catch (EOFException eof) {
//                break;
                continue;
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }

    private Response handleRequest(Request requestObject) throws IOException {
        Operation operation = requestObject.getOperation();
        switch (operation) {
            case LOGIN:
                return login((Map) requestObject.getData()); // for log in we get Map which contains username and password as Strings
            case LOGOUT:
                System.out.println("Klijent - " + this.hashCode() + " se odjavio sa aplikacije");
                this.socket.close();
                break;

            case SAVE_FAMILY_TREE:
                return saveFamilyTree((FamilyTree) requestObject.getData());
            case GET_EXISTING_TREES:
                return getFamilyTrees();
            case GET_MEMBERS_OF_TREE:
                return getMembersOfTree((FamilyTree) requestObject.getData());
            case ADD_NEW_MEMBER:
                return saveMember((Map<String, Object>) requestObject.getData());
            case GET_RELATIONSHIP_TYPES:
                return getRelationshipTypes();

        }
        return null;
    }

    private Response getRelationshipTypes() {
        Response response;
        try {
            RelationshipType[] data = relationshipTypeService.getCloseFamilyRoles();
            response = new Response(data, ResponseStatus.SUCCESS);
        } catch (SQLException e) {
            response = new Response(null, ResponseStatus.SUCCESS, e);
            e.printStackTrace();
        }
        return response;
    }

    private Response saveMember(Map<String, Object> data) {
        Response res;

        Membership membership = (Membership) data.get("membership");
        List<FamilyRelationship> familyRelationships = (List<FamilyRelationship>) data.get("familyMembers");
        try {
            membership = membershipService.addMembership(membership, familyRelationships);

            res = new Response(membership, ResponseStatus.SUCCESS);
            System.out.println("Uspesno cuvanje clana, napravljen zahtev");
        } catch (Exception e) {
            res = new Response(null, ResponseStatus.ERROR, e);
            e.printStackTrace();
        }

        return res;
    }

    private Response getMembersOfTree(FamilyTree tree) {
        Response res;
        try {
            Member[] members = membershipService.getMembersOf(tree);
            res = new Response(members, ResponseStatus.SUCCESS);
        } catch (SQLException e) {
            res = new Response(null, ResponseStatus.ERROR, e);
            e.printStackTrace();
        }
        return res;
    }

    private Response getFamilyTrees() {
        Response response;
        try {
            FamilyTree[] trees = familyTreeService.getTrees();
            response = new Response(trees, ResponseStatus.SUCCESS);
        } catch (SQLException e) {
            e.printStackTrace();
            response = new Response(null, ResponseStatus.ERROR, e);
        }
        return response;
    }

    private Response saveFamilyTree(FamilyTree data) {
        Response response;
        try {
            FamilyTree savedTree = familyTreeService.saveNewFamilyTree(data);
            response = new Response(savedTree, ResponseStatus.SUCCESS);
        } catch (SQLException | StructuralBoundaryException e) {
            e.printStackTrace();
            response = new Response(null, ResponseStatus.ERROR, e);
        }
        return response;
    }

    private Response login(Map<String, String> data) {
        try {
            User user = userService.login(data.get("username"), data.get("password"));
            return new Response(user, ResponseStatus.SUCCESS);
        } catch (UserNotFoundException e) {
            System.err.println(e.getMessage());
            return new Response(null, ResponseStatus.ERROR, e);
        }
    }

    public Socket getSocket() {
        return socket;
    }

//    public BufferedReader getFromClient() {
//        return fromClient;
//    }
//
//    public PrintStream getToClient() {
//        return toClient;
//    }


    public ObjectOutputStream getToClient() {
        return toClient;
    }

    public ObjectInputStream getFromClient() {
        return fromClient;
    }

    public User getLoginUser() {
        return loginUser;
    }
}
