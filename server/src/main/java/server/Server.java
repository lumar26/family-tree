package server;

import model.User;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread {
    private final ServerSocket serverSocket;
    private final List<ClientHandler> clients;

    public Server() throws IOException {
        serverSocket = new ServerSocket(9000);
        clients = new ArrayList<>();
    }

    @Override
    public void run() {
        System.out.println("Server je pokrenut");

        while (!serverSocket.isClosed()) {
            try {
                Socket socket = serverSocket.accept();
                ClientHandler handler = new ClientHandler(socket);
                clients.add(handler);
                handler.start();
                System.out.println("Klijent - " + handler.hashCode() + " se povezao");

            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

        stopAllThreads();
    }


    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    private void stopAllThreads() {
        for (ClientHandler client : clients) {
            try {
                client.getSocket().close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        for (ClientHandler client : clients) {
            users.add(client.getLoginUser());
        }

        return users;
    }
}
