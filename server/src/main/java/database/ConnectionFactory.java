package database;


import util.PropertiesCache;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    private  Connection connection;
    private static ConnectionFactory instance;

    private ConnectionFactory(){
        String url = PropertiesCache.getInstance().getProperty("database_url");
        String username = PropertiesCache.getInstance().getProperty("database_user");
        String password = PropertiesCache.getInstance().getProperty("database_password");

        try {
            connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ConnectionFactory getInstance(){
        return instance == null ? new ConnectionFactory() : instance;
    }

    public Connection getConnection() {
        return connection;
    }
}

