package repository;

import database.ConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface Repository<T, K> {
    Connection connection = ConnectionFactory.getInstance().getConnection();

    T save(T object) throws SQLException;
    List<T> findAll() throws SQLException;
    T findByID(K primaryKey) throws SQLException;
    T delete(K primaryKey) throws SQLException;
    T update(T updatedObject, K primaryKey) throws SQLException;
    T toEntity(ResultSet set) throws SQLException;

    default void commit() throws SQLException {
        connection.commit();
    }

    default void rollback() throws SQLException {
        connection.rollback();
    }
}
