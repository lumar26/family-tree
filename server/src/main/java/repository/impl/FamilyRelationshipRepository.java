package repository.impl;

import model.FamilyRelationship;
import model.Member;
import model.Membership;
import model.RelationshipType;
import repository.Repository;
import repository.key.FamilyRelationshipPrimaryKey;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FamilyRelationshipRepository implements Repository<FamilyRelationship, FamilyRelationshipPrimaryKey> {
    @Override
    public FamilyRelationship save(FamilyRelationship object) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "insert into " + FamilyRelationship.getDBTableName() + " (" + FamilyRelationship.getTableAttributesForInsert() + ") " +
                        "values (?, ?, ?, ?);");
        statement.setLong(1, object.getMembership().getId());
        statement.setLong(2,object.getNumber());
        statement.setLong(3, object.getFamilyMember().getId());
        statement.setLong(4,object.getRelationshipType().getId());


        statement.executeUpdate();
//        ResultSet rs = statement.getGeneratedKeys();
//        if (rs.next()) {
//            object.setId(rs.getLong(1));
//        }
//        connection.commit();
        statement.close();
//        rs.close();
        return object;
    }

    @Override
    public List<FamilyRelationship> findAll() throws SQLException {
        List<FamilyRelationship> memberships = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(
                "select " + FamilyRelationship.getAllTableAttributes() + " from " + FamilyRelationship.getDBTableName() + ";");
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            memberships.add(toEntity(rs));
        }
        statement.close();
        rs.close();
        return memberships;
    }

    @Override
    public FamilyRelationship findByID(FamilyRelationshipPrimaryKey primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "select " + FamilyRelationship.getAllTableAttributes() + " from " + FamilyRelationship.getDBTableName() +
                        " where membership_id = ? and number = ?;");
        statement.setLong(1, primaryKey.getMembershipId());
        statement.setLong(2, primaryKey.getNumber());
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            return toEntity(rs);
        }
        statement.close();
        rs.close();
        return null; // TODO: ovde da se odradi pomocu Exception-a a ne da se vraca null
    }

    @Override
    public FamilyRelationship delete(FamilyRelationshipPrimaryKey primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("delete from " + Membership.getDBTableName() +
                " where membership_id = ? and number = ?;");
        statement.setLong(1, primaryKey.getMembershipId());
        statement.setLong(2, primaryKey.getNumber());

        FamilyRelationship deletedRelationship = findByID(primaryKey);

        statement.executeUpdate();

        statement.close();
        return deletedRelationship;
    }

    @Override
    public FamilyRelationship update(FamilyRelationship updatedObject, FamilyRelationshipPrimaryKey primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("update " + FamilyRelationship.getDBTableName() + " set " +
                "family_member_id = ?, " +
                "relationship_type_id = ? " +
                " where membership_id = ? and number = ?;");
        statement.setLong(1, updatedObject.getFamilyMember().getId());
        statement.setLong(2, updatedObject.getRelationshipType().getId());

        statement.setLong(3, primaryKey.getMembershipId());
        statement.setLong(4, primaryKey.getNumber());

        statement.executeUpdate();
        statement.close();
        return findByID(primaryKey);
    }

    @Override
    public FamilyRelationship toEntity(ResultSet set) throws SQLException {
        Repository<Membership, Long> membershipRepo = new MembershipRepository();
        Repository<RelationshipType, Long> typeRepo = new RelationshipTypeRepository();
        Repository<Member, Long> memberRepo = new MemberRepository();

        Membership membership = membershipRepo.findByID(set.getLong("tree_id"));
        RelationshipType relationshipType = typeRepo.findByID(set.getLong("user_id"));
        Member member = memberRepo.findByID(set.getLong("member_id"));
        return new FamilyRelationship(membership, set.getLong("number"), member, relationshipType);
    }
}
