package repository.impl;

import model.User;
import repository.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository implements Repository<User, Long> {

    @Override
    public User save(User object) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "insert into " + User.getDBTableName() + " (" + User.getTableAttributesForInsert() + ") " +
                        "values (?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
        statement.setString(1, object.getUsername());
        statement.setString(2, object.getPassword());
        statement.setString(3, object.getEmail());
        statement.setString(4, object.getName());
        statement.setString(5, object.getSurname());


        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            object.setId(rs.getLong(1));
        }
        connection.commit();
        statement.close();
        rs.close();
        return object;
    }

    @Override
    public List<User> findAll() throws SQLException {
        List<User> users = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(
                "select " + User.getAllTableAttributes() + " from user");
        ResultSet rs = statement.executeQuery();
        while (rs.next()){
            users.add(toEntity(rs));
        }
        statement.close();
        rs.close();
        return users;
    }

    @Override
    public User findByID(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "select " + User.getAllTableAttributes() + " from " + User.getDBTableName() + " where id = ?;" );
        statement.setLong(1, primaryKey);
        ResultSet rs = statement.executeQuery();
        if (rs.next()){
            return toEntity(rs);
        }
        statement.close();
        rs.close();
        return null; // TODO: ovde da se odradi pomocu ExCeption-a a ne da se vraca null
    }

    @Override
    public User delete(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("delete from " + User.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);

        User deletedUser = findByID(primaryKey);

        statement.executeUpdate();

        statement.close();
        return deletedUser;

    }

    @Override
    public User update(User updatedObject, Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("update user set " +
                "username = ?, " +
                "password = ?, " +
                "email = ?, " +
                "name = ?, " +
                "surname = ? " +
                "where id = ?;");
        statement.setString(1, updatedObject.getUsername());
        statement.setString(2, updatedObject.getPassword());
        statement.setString(3, updatedObject.getEmail());
        statement.setString(4, updatedObject.getName());
        statement.setString(5, updatedObject.getSurname());
        statement.setLong(6, primaryKey);

        statement.executeUpdate();
        statement.close();
        return findByID(primaryKey);
    }

    @Override
    public User toEntity(ResultSet rs) throws SQLException {
        return new User(rs.getLong("id"), rs.getString("username"), rs.getString("password"),
                rs.getString("email"), rs.getString("name"), rs.getString("surname"));
    }
}
