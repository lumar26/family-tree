package repository.impl;

import model.FamilyTree;
import model.User;
import repository.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FamilyTreeRepository implements Repository<FamilyTree, Long> {
    @Override
    public FamilyTree save(FamilyTree object) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "insert into " + FamilyTree.getDBTableName() + " (" + FamilyTree.getTableAttributesForInsert() + ") " +
                        "values (?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
        statement.setString(1, object.getName());
        statement.setString(2, object.getDescription());
        statement.setDate(3, Date.valueOf(object.getDateFrom()));
        statement.setLong(4, object.getUser().getId());


        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            object.setId(rs.getLong(1));
        }
//        connection.commit();
        statement.close();
        rs.close();
        return object;
    }

    @Override
    public List<FamilyTree> findAll() throws SQLException {
        List<FamilyTree> trees = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(
                "select " + FamilyTree.getAllTableAttributes() + " from " + FamilyTree.getDBTableName() + ";");
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            trees.add(toEntity(rs));
        }
        statement.close();
        rs.close();
        return trees;
    }

    @Override
    public FamilyTree findByID(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "select " + FamilyTree.getAllTableAttributes() + " from " + FamilyTree.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            return toEntity(rs);
        }
        statement.close();
        rs.close();
        return null; // TODO: ovde da se odradi pomocu ExCeption-a a ne da se vraca null
    }

    @Override
    public FamilyTree delete(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("delete from " + FamilyTree.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);

        FamilyTree deletedTree = findByID(primaryKey);

        statement.executeUpdate();

        statement.close();
        return deletedTree;
    }

    @Override
    public FamilyTree update(FamilyTree updatedObject, Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("update " + FamilyTree.getDBTableName() + " set " +
                "name = ?, " +
                "description = ?, " +
                "date_from = ?, " +
                "user_id = ? " +
                "where id = ?;");
        statement.setString(1, updatedObject.getName());
        statement.setString(2, updatedObject.getDescription());
        statement.setDate(3, Date.valueOf(updatedObject.getDateFrom()));
        statement.setLong(4, updatedObject.getUser().getId());
        statement.setLong(5, primaryKey);

        statement.executeUpdate();
        statement.close();
        return findByID(primaryKey);
    }

    @Override
    public FamilyTree toEntity(ResultSet set) throws SQLException {
        Repository<User, Long> userRepo = new UserRepository();
        User user = userRepo.findByID(set.getLong("user_id"));
        LocalDate dateFrom = set.getDate("date_from").toLocalDate();
        return new FamilyTree(set.getLong("id"), set.getString("name"), set.getString("description"), dateFrom, user);
    }
}
