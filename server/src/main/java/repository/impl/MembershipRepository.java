package repository.impl;

import model.FamilyTree;
import model.Member;
import model.Membership;
import model.User;
import repository.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MembershipRepository implements Repository<Membership, Long> {
    @Override
    public Membership save(Membership object) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "insert into " + Membership.getDBTableName() + " (" + Membership.getTableAttributesForInsert() + ") " +
                        "values (?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
        statement.setDate(1, Date.valueOf(object.getCreationDate()));
        statement.setLong(2, object.getFamilyTree().getId());
        statement.setLong(3, object.getUser().getId());
        statement.setLong(4, object.getMember().getId());


        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            System.out.println("Saved membership with id=" + rs.getLong(1));
            object.setId(rs.getLong(1));
        }
//        connection.commit();
        statement.close();
        rs.close();
        return object;
    }

    @Override
    public List<Membership> findAll() throws SQLException {
        List<Membership> memberships = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(
                "select " + Membership.getAllTableAttributes() + " from " + Membership.getDBTableName() + ";");
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            memberships.add(toEntity(rs));
        }
        statement.close();
        rs.close();
        return memberships;
    }

    @Override
    public Membership findByID(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "select " + Membership.getAllTableAttributes() + " from " + Membership.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            return toEntity(rs);
        }
        statement.close();
        rs.close();
        return null; // TODO: ovde da se odradi pomocu Exception-a a ne da se vraca null
    }

    @Override
    public Membership delete(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("delete from " + Membership.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);

        Membership deletedMembership = findByID(primaryKey);

        statement.executeUpdate();

        statement.close();
        return deletedMembership;
    }

    @Override
    public Membership update(Membership updatedObject, Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("update " + Membership.getDBTableName() + " set " +
                "creation_date = ?, " +
                "tree_id = ?, " +
                "user_id = ?, " +
                "member_id = ? " +
                "where id = ?;");
        statement.setDate(1, Date.valueOf(updatedObject.getCreationDate()));

        statement.setLong(2, updatedObject.getFamilyTree().getId());
        statement.setLong(3, updatedObject.getUser().getId());
        statement.setLong(4, updatedObject.getMember().getId());

        statement.setLong(5, primaryKey);

        statement.executeUpdate();
        statement.close();
        return findByID(primaryKey);
    }

    @Override
    public Membership toEntity(ResultSet set) throws SQLException {
        Repository<FamilyTree, Long> treeRepo = new FamilyTreeRepository();
        Repository<User, Long> userRepo = new UserRepository();
        Repository<Member, Long> memberRepo = new MemberRepository();

        FamilyTree tree = treeRepo.findByID(set.getLong("tree_id"));
        User user = userRepo.findByID(set.getLong("user_id"));
        Member member = memberRepo.findByID(set.getLong("member_id"));


        return new Membership(set.getLong("id"), set.getDate("creation_date").toLocalDate(), tree, user, member);
    }
}
