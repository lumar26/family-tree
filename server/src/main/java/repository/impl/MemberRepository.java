package repository.impl;

import model.Member;
import model.enums.Gender;
import repository.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MemberRepository implements Repository<Member, Long> {
    @Override
    public Member save(Member object) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "insert into " + Member.getDBTableName() + " (" + Member.getTableAttributesForInsert() + ") " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
        statement.setString(1, object.getName());
        statement.setString(2, object.getSurname());
        statement.setDate(3, Date.valueOf(object.getBirthDate()));
        statement.setDate(4, object.getDeathDate() == null ? null : Date.valueOf(object.getDeathDate()));
        statement.setString(5, object.getGender().toString());
        statement.setString(6, object.getBirthPlace());
        statement.setString(7, object.getDeathPlace());
        statement.setString(8, object.getAddress());
        statement.setString(9, object.getBiographyUrl());
        statement.setString(10, object.getPictureUrl());


        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            object.setId(rs.getLong(1));
        }
//        connection.commit();
        statement.close();
        rs.close();
        return object;
    }

    @Override
    public List<Member> findAll() throws SQLException {
        List<Member> members = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(
                "select " + Member.getAllTableAttributes() + " from " + Member.getDBTableName() + ";");
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            members.add(toEntity(rs));
        }
        statement.close();
        rs.close();
        return members;
    }

    @Override
    public Member findByID(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "select " + Member.getAllTableAttributes() + " from " + Member.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            return toEntity(rs);
        }
        statement.close();
        rs.close();
        return null; // TODO: ovde da se odradi pomocu Exception-a a ne da se vraca null
    }

    @Override
    public Member delete(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("delete from " + Member.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);

        Member deletedMember = findByID(primaryKey);

        statement.executeUpdate();

        statement.close();
        return deletedMember;
    }

    @Override
    public Member update(Member updatedObject, Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("update " + Member.getDBTableName() + " set " +
                "name = ?, " +
                "surname = ?, " +
                "birth_date = ?, " +
                "death_date = ?, " +
                "gender = ?, " +
                "birth_place = ?, " +
                "death_place = ?, " +
                "address = ?, " +
                "biography_url = ?, " +
                "picture_url = ? " +
                "where id = ?;");
        statement.setString(1, updatedObject.getName());
        statement.setString(2, updatedObject.getSurname());
        statement.setDate(3, Date.valueOf(updatedObject.getBirthDate()));
        statement.setDate(4, Date.valueOf(updatedObject.getDeathDate()));
        statement.setString(5, updatedObject.getGender().toString());
        statement.setString(6, updatedObject.getBirthPlace());
        statement.setString(7, updatedObject.getDeathPlace());
        statement.setString(8, updatedObject.getAddress());
        statement.setString(9, updatedObject.getBiographyUrl());
        statement.setString(10, updatedObject.getPictureUrl());
        statement.setLong(5, primaryKey);

        statement.executeUpdate();
        statement.close();
        return findByID(primaryKey);
    }

    @Override
    public Member toEntity(ResultSet set) throws SQLException {

//                datum smrti je opciono polje pa moze biti i null
        LocalDate deathDate = set.getDate("death_date") == null ? null : set.getDate("death_date").toLocalDate();

        return new Member(set.getLong("id"), set.getString("name"), set.getString("surname"),
                set.getDate("birth_date").toLocalDate(), deathDate,
                Gender.valueOf(set.getString("gender")), set.getString("birth_place"), set.getString("death_place"),
                set.getString("address"), set.getString("biography_url"), set.getString("picture_url"));
    }
}
