package repository.impl;

import model.RelationshipType;
import repository.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RelationshipTypeRepository implements Repository<RelationshipType, Long> {
    @Override
    public RelationshipType save(RelationshipType object) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "insert into " + RelationshipType.getDBTableName() + " (" + RelationshipType.getTableAttributesForInsert() + ") " +
                        "values (?);", PreparedStatement.RETURN_GENERATED_KEYS);
        statement.setString(1, object.getName());


        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            object.setId(rs.getLong(1));
        }
//        connection.commit();
        statement.close();
        rs.close();
        return object;
    }

    @Override
    public List<RelationshipType> findAll() throws SQLException {
        List<RelationshipType> types = new ArrayList<>();
//        PreparedStatement statement = connection.prepareStatement(
//                "select " + RelationshipType.getAllTableAttributes() + " from " + RelationshipType.getDBTableName() + ";");
        PreparedStatement statement = connection.prepareStatement(
                "select id, name from relationship_type;");
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            types.add(toEntity(rs));
        }
        statement.close();
        rs.close();
        return types;
    }

    @Override
    public RelationshipType findByID(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "select " + RelationshipType.getAllTableAttributes() + " from " + RelationshipType.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            return toEntity(rs);
        }
        statement.close();
        rs.close();
        return null; // TODO: ovde da se odradi pomocu ExCeption-a a ne da se vraca null
    }

    @Override
    public RelationshipType delete(Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("delete from " + RelationshipType.getDBTableName() + " where id = ?;");
        statement.setLong(1, primaryKey);

        RelationshipType deletedType = findByID(primaryKey);

        statement.executeUpdate();

        statement.close();
        return deletedType;
    }

    @Override
    public RelationshipType update(RelationshipType updatedObject, Long primaryKey) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("update " + RelationshipType.getDBTableName() + " set " +
                "name = ? " +
                "where id = ?;");
        statement.setString(1, updatedObject.getName());
        statement.setLong(2, primaryKey);

        statement.executeUpdate();
        statement.close();
        return findByID(primaryKey);
    }

    @Override
    public RelationshipType toEntity(ResultSet set) throws SQLException {
        return new RelationshipType(set.getLong("id"), set.getString("name"));
    }
}
