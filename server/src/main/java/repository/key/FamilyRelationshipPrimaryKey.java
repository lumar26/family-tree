package repository.key;

import model.FamilyRelationship;

public class FamilyRelationshipPrimaryKey {
    private final long membershipId;
    private final long number;

    public FamilyRelationshipPrimaryKey(FamilyRelationship fm) {
        this.membershipId = fm.getMembership().getId();
        this.number = fm.getNumber();
    }

    public long getMembershipId() {
        return membershipId;
    }

    public long getNumber() {
        return number;
    }
}
