package exception;

import exception.enums.BoundaryType;
import exception.enums.StructuralOperation;

public class StructuralBoundaryException extends Exception{
    private BoundaryType boundaryType;
    private StructuralOperation operation;
    private String entity;

    public StructuralBoundaryException(String entity, BoundaryType boundaryType, StructuralOperation operation) {
        super(String.format("Prekršeno strukturno ograničenje tipa: %s pri operaciji: %s  za entitet: %s", boundaryType.toString(), operation.toString(), entity ));
        this.boundaryType = boundaryType;
        this.operation = operation;
        this.entity = entity;
    }

    public BoundaryType getBoundaryType() {
        return boundaryType;
    }

    public StructuralOperation getOperation() {
        return operation;
    }

    public String getEntity() {
        return entity;
    }
}
