package exception.enums;

public enum StructuralOperation {
    INSERT, UPDATE, DELETE
}
