package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize
public class RelationshipType implements Serializable {
    private long id;
    private String name;

    public RelationshipType() {
    }

    public RelationshipType(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public RelationshipType(String name) {
        this.name = name;
    }

    public static String getDBTableName() {
        return "relationship_type";
    }

    public static String getTableAttributesForInsert(){
        return "name";
    }

    public static String getAllTableAttributes(){return "id, " + getTableAttributesForInsert();}

    @Override
    public String toString() {
        return "Uloga - " + name ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RelationshipType)) return false;
        RelationshipType that = (RelationshipType) o;
        return getId() == that.getId() && getName().equals(that.getName());
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
