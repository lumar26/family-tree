package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.time.LocalDate;

@JsonSerialize
public class FamilyTree implements Serializable {
    private long id;
    private String name;
    private String description;
    private LocalDate dateFrom;
    private User user;

    public FamilyTree() {
    }

    public FamilyTree(long id, String name, String description, LocalDate dateFrom, User user) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateFrom = dateFrom;
        this.user = user;
    }

    public FamilyTree(String name, String description, LocalDate dateFrom, User user) {
        this.name = name;
        this.description = description;
        this.dateFrom = dateFrom;
        this.user = user;
    }

    public static String getDBTableName() {
        return "tree";
    }

    public static String getTableAttributesForInsert(){
        return "name, description, date_from, user_id";
    }

    public static String getAllTableAttributes(){return "id, " + getTableAttributesForInsert();}

    @Override
    public String toString() {
        return "FamilyTree - " + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FamilyTree)) return false;
        FamilyTree that = (FamilyTree) o;
        return getId() == that.getId() && getName().equals(that.getName()) && getUser().equals(that.getUser());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
