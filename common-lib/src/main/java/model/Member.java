package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import model.enums.Gender;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@JsonSerialize
public class Member implements Serializable {
    private long id;
    private String name;
    private String surname;
    private LocalDate birthDate;
    private LocalDate deathDate;
    private Gender gender;
    private String birthPlace;
    private String deathPlace;
    private String address;
    private String biographyUrl;
    private String pictureUrl;

    public Member() {
    }

    public Member(long id, String name, String surname,
                  LocalDate birthDate, LocalDate deathDate,
                  Gender gender, String birthPlace, String deathPlace,
                  String address, String biographyUrl, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.gender = gender;
        this.birthPlace = birthPlace;
        this.deathPlace = deathPlace;
        this.address = address;
        this.biographyUrl = biographyUrl;
        this.pictureUrl = pictureUrl;
    }

    public Member(String name, String surname, LocalDate birthDate, LocalDate deathDate,
                  Gender gender, String birthPlace, String deathPlace, String address,
                  String biographyUrl, String pictureUrl) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.gender = gender;
        this.birthPlace = birthPlace;
        this.deathPlace = deathPlace;
        this.address = address;
        this.biographyUrl = biographyUrl;
        this.pictureUrl = pictureUrl;
    }


    public static String getDBTableName() {
        return "member";
    }

    public static String getTableAttributesForInsert(){
        return "name, surname, birth_date, death_date, gender, birth_place, death_place, address, biography_url, picture_url";
    }

    public static String getAllTableAttributes(){return "id, " + getTableAttributesForInsert();}

    @Override
    public String toString() {
        return "Member{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;
        Member member = (Member) o;
        return getId() == member.getId() && Objects.equals(getName(), member.getName()) && Objects.equals(getSurname(), member.getSurname()) && getGender() == member.getGender();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getDeathPlace() {
        return deathPlace;
    }

    public void setDeathPlace(String deathPlace) {
        this.deathPlace = deathPlace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBiographyUrl() {
        return biographyUrl;
    }

    public void setBiographyUrl(String biographyUrl) {
        this.biographyUrl = biographyUrl;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
