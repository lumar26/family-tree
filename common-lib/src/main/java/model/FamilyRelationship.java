package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize
public class FamilyRelationship implements Serializable {
    private  Membership membership; // ovim se oslikava kompozicija
    private long number;
    private Member familyMember;
    private RelationshipType relationshipType;

    public FamilyRelationship() {
    }

    public FamilyRelationship(Membership membership, long number, Member familyMember, RelationshipType relationshipType) {
        this.membership = membership;
        this.number = number;
        this.familyMember = familyMember;
        this.relationshipType = relationshipType;
    }


    public static String getDBTableName() {
        return "family_relationship";
    }

    public static String getTableAttributesForInsert(){
        return getAllTableAttributes();
    }

    public static String getAllTableAttributes(){return "membership_id, number, family_member_id, relationship_type_id";}

    public Membership getMembership() {
        return membership;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Member getFamilyMember() {
        return familyMember;
    }

    public void setFamilyMember(Member familyMember) {
        this.familyMember = familyMember;
    }

    public RelationshipType getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(RelationshipType relationshipType) {
        this.relationshipType = relationshipType;
    }
}
