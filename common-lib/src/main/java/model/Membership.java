package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@JsonSerialize
public class Membership implements Serializable {
    private long id;
    private LocalDate creationDate;
    private FamilyTree familyTree;
    private User user;
    private Member member;
    private List<FamilyRelationship> relationships;

    public Membership() {
    }

    public Membership(long id, LocalDate creationDate, FamilyTree familyTree, User user, Member member) {
        this.id = id;
        this.creationDate = creationDate;
        this.familyTree = familyTree;
        this.user = user;
        this.member = member;
    }

    public Membership(LocalDate creationDate, FamilyTree familyTree, User user, Member member) {
        this.creationDate = creationDate;
        this.familyTree = familyTree;
        this.user = user;
        this.member = member;
        this.relationships = new ArrayList<>();
    }

    public Membership(long id, LocalDate creationDate, FamilyTree familyTree, User user, Member member, List<FamilyRelationship> relationships) {
        this.id = id;
        this.creationDate = creationDate;
        this.familyTree = familyTree;
        this.user = user;
        this.member = member;
        this.relationships = relationships;
    }

    public static String getDBTableName() {
        return "membership";
    }

    public static String getTableAttributesForInsert(){
        return "creation_date, tree_id, user_id, member_id";
    }

    public static String getAllTableAttributes(){return "id, " + getTableAttributesForInsert();}

    @Override
    public String toString() {
        return "Membership{" +
                "familyTree=" + familyTree +
                ", member=" + member +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Membership)) return false;
        Membership that = (Membership) o;
        return getId() == that.getId() && getFamilyTree().equals(that.getFamilyTree()) && getMember().equals(that.getMember());
    }

    public List<FamilyRelationship> getRelationships() {
        return relationships;
    }

    public void setRelationships(List<FamilyRelationship> relationships) {
        this.relationships = relationships;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public FamilyTree getFamilyTree() {
        return familyTree;
    }

    public void setFamilyTree(FamilyTree familyTree) {
        this.familyTree = familyTree;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
