package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class PropertiesCache {
    private final Properties props;

    public PropertiesCache() {
        this.props = new Properties();
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("application.properties");
        try {
            props.load(is);
        } catch (IOException e) {
//            e.printStackTrace();
            System.err.println("Could not load properties");
        }
    }
    //  Bill Pugh implementation of Singleton pattern
    private static class LazyHolder {
        private static final PropertiesCache INSTANCE = new PropertiesCache();
    }

    public static PropertiesCache getInstance() {
        return LazyHolder.INSTANCE;
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public Set<String> getAllPropertyNames() {
        return props.stringPropertyNames();
    }

    public boolean containsKey(String key) {
        return props.containsKey(key);
    }
}
