package network;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize
public class Request implements Serializable {
    private final Object data;
    private final Operation operation;

    public Request(Object data, Operation operation) {
        this.data = data;
        this.operation = operation;
    }

    public Object getData() {
        return data;
    }

    public Operation getOperation() {
        return operation;
    }
}
