package network;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize
public enum Operation implements Serializable {
    LOGIN,
    SAVE_FAMILY_TREE,
    GET_MEMBERS_OF_TREE,
    GET_EXISTING_TREES,
    ADD_NEW_MEMBER,
    LOGOUT, GET_RELATIONSHIP_TYPES

}
