package network.data;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Map;

@JsonSerialize
public class LoginMap implements Serializable {
    private final Map<String, String> loginData;

    public LoginMap(Map<String, String> loginData) {
        this.loginData = loginData;
    }

    public Map<String, String> getLoginData() {
        return loginData;
    }
}
